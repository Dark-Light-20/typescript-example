# TypeScript
This examples are based on these tutorials:
1. https://www.youtube.com/playlist?list=PL4cUxeGkcC9gUgr39Q_yD6v-bSyMwKPUI
2. https://www.youtube.com/watch?v=BwuLxPH8IDs (more detailed)

For TypeScript must have installed NodeJS (because we need NPM)

## Installation:

1. npm install -g typescript
2. tsc --init 

    ((tsconfig file --> 
        change to es6
        outDir
        rootDir))

    "sourcemap": true       //  Enables to link ts with js in modern browsers for debuggin

    "removeComments": true  // For compiled JS

## Extensions for vscode
* EsLint
* TSLint

and install:

* npm -D typescript ts-node nodemon @types/node @types/express

## TypeScript types:
1. tuple    (fixed length/type array)
2. enum     (dictionary)
3. any      (any type xD)

## extra Types:
1. Union types      (number | string) --> for func params
2. Literal types    (specify)
3. Type aliasses    (type <name>: number | string)
4. Optional | Default params     (a: number, b: number, c?: number = 10)
5. implements interfaces (for classes)