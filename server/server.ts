// Imports
import express from 'express';
import path from 'path';

// Server instance
const app = express();

// Router
app.use(require('./routes/examples.ts'));

// Load utilities
app.use(express.static(path.join(__dirname, '../', './static')));
app.set('views', 'views');
app.set('view engine', 'pug');

app.listen(4000, () => {
  console.log('Running on port 4000');
});
