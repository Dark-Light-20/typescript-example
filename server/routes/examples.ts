import express from 'express';

const router = express.Router();

router.get('/', (req, res) => {
  res.send('HOME :3');
});

router.get('/example1', (req, res) => {
  res.render('example1');
});

router.get('/example2', (req, res) => {
  res.render('example2');
});

module.exports = router;
