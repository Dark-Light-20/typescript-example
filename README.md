# Review TypeScript

TypeScript es un superset de JavaScript, osea que busca extender las funcionalidades de JavaScript, y tal como lo sugiere su nombre busca implementar un tipado estático, que es opcional al lenguaje. 

Basándose en la filosofía, de que los errores deben ser visibles desde la etapa de desarrollo. Además de mejorar la legibilidad del código. Ya que el código TypeScript no es entendido por los navegadores, este se transpira a JavaScript. 

## Tipado Básico con TypeScript

Una de las características básicas de Typescript es el uso de tipado estático. Dando soporte a todos los tipos de variables utilizados dentro de JavaScript como:

    • boolean: permite valores true/false
    • number: permite valores enteros, flotantes, binarios, octales y hexadecimales
    • string: cadenas delimitadas por “”, ‘ ’ y ` `
    • array: listas delimitadas por [ ], pueden declararse con tipos sencillos seguidos por [] o utilizando array<tipo>.
    • tuple: permite establecer un formato para arrays de tamaño fijo, tal como la posición y tipo de sus elementos.
    • enum: Similar a una enumeración tradicional
    • any: Permite el uso de una variable para que esta pueda tomar cualquier valor de cualquier tipo. 
    • void: Indica que un elemento debe ser vacío, generalmente se usa para indicar que una función no tiene retorno.

Entre otros tipos que son permitidos por TypeScript, la mayoría siguen una sintaxis similar:

var|let|const < varname > : < vartype > = < value >;

## Interfaces con TypeScript

Otro de los principios de Typescript es el chequeo del tipo de variables con respecto a la “forma” de sus valores. 
Para entender su funcionamiento revisemos el ejemplo:

```typescript
function printLabel(labeledObj: { label: string }) {
    console.log(labeledObj.label);
}

let myObj = {size: 10, label: "Size 10 Object"};
printLabel(myObj);
```

Esta función tiene la intención de imprimir el atributo label de  un parámetro, condicionando que el parámetro entrante contenga este atributo y sea de tipo String. En este caso esto podría ser un tanto limitante por lo cual es posible implementar una Interfaz que cumpla esta funcion asi:

```typescript
interface LabeledValue {
    label: string;
}

function printLabel(labeledObj: LabeledValue) {
    console.log(labeledObj.label);
}

let myObj = {size: 10, label: "Size 10 Object"};
printLabel(myObj);
```

Tenemos así, la misma funcionalidad implementando las interfaces de Typescript, las que nos permiten una mayor holgura en cuanto al uso de funciones y parámetros. 

## Clases con TypeScript
Generalmente JavaScript usa funciones y herencia basada en prototipos para modelar clases. Pero esto solo resulta en una implementación extraña para el programador. Si bien estas funcionalidades están disponibles desde ecma 6, TypeScript permite el uso de programación orientada a objetos de una manera más convencional y práctica.

```typescript
class Greeter {
    greeting: string;
    constructor(message: string) {
        this.greeting = message;
    }
    greet() {
        return "Hello, " + this.greeting;
    }
}

let greeter = new Greeter("world");
```

Como podrá observar utilizamos una sintaxis similar a la de C#, permitiendo la aplicación de propiedades comunes, como constructores y encapsulamiento. 

## Funciones

En TypeScript, aunque hay clases, espacios de nombres y módulos, las funciones siguen desempeñando un papel clave en la descripción de cómo hacer las cosas.

Para comenzar, al igual que en JavaScript, las funciones TypeScript se pueden crear como una función con nombre o como una función anónima. Esto le permite elegir el enfoque más apropiado para su aplicación, ya sea que esté creando una lista de funciones en una API o una función única para pasar a otra función.

```typescript
function add(x, y) {
    return x + y;
}

let myAdd = function(x, y) { return x + y; };
```

Al mismo tiempo estas pueden implementar tipos:

```typescript
function add(x: number, y: number): number {
    return x + y;
}

let myAdd = function(x: number, y: number): number { return x + y; };
```

Entre otras funcionalidades que nos permite el uso de TypeScript.

Para mayor informacion consulte: 
https://www.typescriptlang.org/docs/home.html

### Extra Info:

This example is based on these tutorials:
1. https://www.youtube.com/playlist?list=PL4cUxeGkcC9gUgr39Q_yD6v-bSyMwKPUI
2. https://www.youtube.com/watch?v=BwuLxPH8IDs (more detailed)

For TypeScript must have installed NodeJS (because we need NPM)

## Installation:

1. npm install -g typescript
2. tsc --init 

    ((tsconfig file --> 
        change to es6
        outDir
        rootDir))

    "sourcemap": true       //  Enables to link ts with js in modern browsers for debuggin

    "removeComments": true  // For compiled JS

## Extensions for vscode
* EsLint
* TSLint

and install:

* npm -D typescript ts-node nodemon @types/node @types/express

## TypeScript types:
1. tuple    (fixed length/type array)
2. enum     (dictionary)
3. any      (any type xD)

## extra Types:
1. Union types      (number | string) --> for func params
2. Literal types    (specify)
3. Type aliasses    (type <name>: number | string)
4. Optional | Default params     (a: number, b: number, c?: number = 10)
5. implements interfaces (for classes)