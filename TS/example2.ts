// ------ Classes --------

class Person {
  private name: string;

  private age: number;

  private hobbies: string[];

  readonly ptype: string;

  constructor(name: string, age: number) {
    this.name = name;
    this.age = age;
    this.hobbies = [];
    this.ptype = 'Person';
  }

  public addHobies(hobbies: string[]) {
    this.hobbies = hobbies;
  }

  show() {
    return `I'm ${this.name}, ${this.age} years old. I like ${this.hobbies}`;
  }
}

const cBtn = document.getElementById('c_btn')!;
cBtn.addEventListener('click', () => {
  const p1 = new Person('Luis', 18);
  const p2 = new Person('Andres', 20);

  p1.addHobies(['Music', 'Travel']);
  // p2.addHobies(["Draw","Run",10]);
  p2.addHobies(['Draw', 'Run']);

  document.getElementById('person1')!.innerHTML = p1.show();
  document.getElementById('person2')!.innerHTML = p2.show();

  console.log(p1);
  console.log(p2);

  console.log(p1.ptype);

  (cBtn as HTMLInputElement).disabled = true;

  // ---- Encapsulation ----
  // p1.name = "Juan";
  // p1.type = 'Human';
});

// ------ Interfaces --------

interface someone {
  name: string;
  age: number;
  hobbies: string[];
  speak(): string;
}

const p3: someone = {
  name: 'Alex',
  age: 21,
  hobbies: ['Code', 'Games'],
  // hobbies: ['Code', 'Games', 10],    // hobbies: any[];
  speak() {
    return `I'm ${this.name}, ${this.age} years old. I like ${this.hobbies}`;
  }
};

const iBtn = document.getElementById('i_btn')!;
iBtn.addEventListener('click', () => {
  const name = document.getElementById('name')!;
  name.innerHTML = `Name: ${p3.name}`;
  const age = document.getElementById('age')!;
  // age.innerHTML = "Age: "+p3.age;
  age.innerHTML = `Age: ${String(p3.age)}`;
  document.getElementById('hobbies')!.innerHTML = `Hobbies: ${p3.hobbies}`;
  console.log(p3.speak());
  // btn2!.disabled = true;
  (iBtn as HTMLInputElement).disabled = true;
});
