function sum(n1: number, n2: number) {
  return n1 + n2;
}

const btn1 = document.getElementById('btn');
if (btn1 !== null) {
  btn1.addEventListener('click', () => {
    const n1 = (<HTMLInputElement>document.getElementById('n1')).value;
    // const n1 = document.getElementById("n1").value;
    const n2 = (<HTMLInputElement>document.getElementById('n2')).value;
    // n2 = 5;
    const result = sum(Number(n1), Number(n2));
    // const result = sum(n1, n2);
    const txtResult = document.getElementById('result');
    if (txtResult !== null) txtResult.innerHTML = `El resultado es: ${result}`;
  });
}
